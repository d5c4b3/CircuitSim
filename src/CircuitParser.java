import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;


public class CircuitParser {
	
	static final int MAX_TRUTH_TABLE_INPUTS = 16;
	static final int MAX_TRUTH_TABLE_OUTPUTS = 32;
	static String keywords[] = { 
		"inputNames", "outputNames", "outputs", "truthTable", 
		"circuit", "circuitInputs", "propagationDelay"
	};
	transient Scanner scan;
	transient String extraToken;
	transient String repeatHead, repeatTail;
	transient int repeatStart, repeatEnd, repeatVal;
	
	transient boolean repeating;
	transient ArrayList<String> repeatTokens = new ArrayList<>();
	
	Circuit cir;
	
	
	public CircuitParser(Circuit cir) {
		this.cir = cir;
	}
	
	
	void error(String s)
	{
		System.out.println(s);
		System.exit(0);
	}
	
	//Sets scan = circuitType + ".txt"
	void openCircuitFile(String circuitType)
	{
		try {
			File file = new File(circuitType + ".txt");
			scan = new Scanner(file);
			scan.useDelimiter("[ ,{;=\t\n\r]"); // : and treated separately
		} catch (Exception ex) {
			error("\nError opening file '" + circuitType + ".txt'\n");
		}
	}
	
	String getRepeatToken()
	{
		
		if (repeatTokens.size() > 1) {
			return repeatTokens.remove(0); //pops off the top token and returns it
		} else if (repeatTokens.size() == 1){
			repeating = false;
			return repeatTokens.remove(0);
		} else {
			error("Error parsing macro ");
		}
		
		return null;
	}
	
	void initRepeatToken(String s)
	{ //This could probably be shortened
		
		try {
			String token = "";
			ArrayList<String> tmpRepeatTokens = new ArrayList<>();
			
			if (!s.startsWith("(")) {  //case word(word
				int paren = s.indexOf("(");
				String beforeHead = s.substring(0, paren);
				repeatTokens.add(beforeHead);
				s = s.substring(paren);
			}
			
			if (s.contains(")[")) { //if statement skips the loop if we already have the end of the macro head
				token = s.substring(1); //remove the '('
			} 
			else {
				tmpRepeatTokens.add(s.substring(1)); //remove the '('
				
				boolean foundEnd = false;
				
				while (scan.hasNext()) {
					token = scan.next();
					if (token.contains(")")) {
						foundEnd = true;
						break;
					} else {
						tmpRepeatTokens.add(token);
					}
				}
				if (!foundEnd) { error("(1) Illegal macro start at "  + s);}
			}
			
			String[] endHead = token.split("\\)\\["); // "word)[5:0]" get split into "word, 5:0]"
			if (endHead.length != 2) {error("(2) Illegal macro start at "  + s);} 
			tmpRepeatTokens.add(endHead[0]);
			
			String tail = endHead[1];
			if (!tail.contains("]")) { error("(3) No spaces or commas allowed in macro tail: " + s);}
			String[] endTail = tail.split("\\]");
			String afterTail = "";
			if (endTail.length > 1) {afterTail = endTail[1];} //just in case someone does "word)[5:0]word"
			tail = endTail[0];
			int repeatStart = 0;
			int repeatEnd;
			if (tail.contains(":")) {
				String[] tailNums = tail.split(":");
				repeatStart = Integer.parseInt(tailNums[0]);
				repeatEnd = Integer.parseInt(tailNums[1]);
			} else {
				repeatEnd = Integer.parseInt(tail);
			}
			
			
			//So now we have the string to repeat in the repeatTokend arraylist but we haven't actually repeated it yet
			if (repeatEnd == 0 && repeatStart == 0) { return; } //idk why but someone will probably type [0] or [0:0]
			String[] toRepeat = tmpRepeatTokens.toArray(new String[tmpRepeatTokens.size()]);
			int i = repeatStart;
			while(true) { //we want to grab all of the numbers including when i == repeatEnd
		
				for (int n = 0; n < toRepeat.length; n++) {
					String repeatToken = toRepeat[n];
					if (repeatToken.contains("%")) {
						repeatToken = repeatToken.replaceAll("%", i+"");
					}
					repeatTokens.add(repeatToken);
				}
				
				if (repeatStart < repeatEnd) {
					i++;
					if (i > repeatEnd) {break;}
				} else {
					i--;
					if (i < repeatEnd) {break;}
				}
				
			}
			if (!afterTail.isEmpty()) {
				repeatTokens.add(afterTail);
			}
			
			repeating = true;
//			System.out.println("Tokens we are repeating: ");
//			for (int e = 0; e < repeatTokens.size(); e++) {
//				System.out.print(repeatTokens.get(e) +" ");
//			}
//			System.out.println("");
			//now everything should be done
			
		} catch (Exception e) {
			e.printStackTrace();
			error("Error. Illegal repeater: " + s);
		}
	}
	
	String getToken()
	{
		if (repeating) {
			return getRepeatToken();
		}
		else if (extraToken != null) {
			String t = extraToken;
			extraToken = null;
			return t;
		}
		while (scan.hasNext()) {
			String token = scan.next();
			if (token.length() == 0) {
				continue;
			}
			else if (token.startsWith("#")) {
				scan.nextLine();
			}
			else if (token.contains("(")) {
				initRepeatToken(token);
				return getRepeatToken();
			}
			else if (token.equals("}")) {
				return token;
			}
			else if (token.endsWith("}")) {
				extraToken = "}";
				return token.substring(0, token.length()-1);
			}
			else if (token.isEmpty()) {
				continue;
			}
			else {
				return token;
			}
		}
		return null;
	}
	
	void ungetToken(String token) 
	{
		extraToken = token; // currently can only unget 1 token
	}
	
	boolean isKeyword(String s)
	{
		for (int i=0; i<keywords.length; i++) {
			if (keywords[i].equals(s)) return true;
		}
		return false;
	}
	
	void loadStringList(ArrayList<String> stringList)
	{
		String token;
		while ((token = getToken()) != null) {
			if (isKeyword(token)) {
				ungetToken(token);
				break;
			}
			if (token.equals("}")) break;
			stringList.add(token);
		}
	}
	
	boolean getArrayAndIndex(ArrayIndex ai, String str) //I still don't know what this function is supposed to do.
	{
		if (cir != null) {
			
			
			ai.arr = null;
			ai.idx = -1;
			
			// See if we are an input
			int index = cir.inputNames.indexOf(str);
			if (index >= 0) {
				ai.arr = cir.inputs;
				ai.idx = index;
				return true;
			}
			
			// Try to find an output of a subunit
			int dotIndex = str.indexOf('.');
			String strSuffix = null; // Whatever is past the dot
			if (dotIndex>0) strSuffix = str.substring(dotIndex+1);
			
			for (int j=0; j<cir.internalCircuits.size(); j++) {
				Circuit d = cir.internalCircuits.get(j);
				if (str.equals(d.name) || str.startsWith(d.name+".")) {
					ai.arr = d.outputs;
					if (strSuffix == null) { // no suffix, so use index 0
						if (d.outputNames.size() > 1) {
							error("\nError: unqualified circuit output for circuit "
								+ "with multiple outputs: '" + str + "'\n");
						}
						ai.idx = 0; 
						return true;
					}
					else {
						// First check for a name
						ai.idx = d.outputNames.indexOf(strSuffix);
						if (ai.idx >= 0) {
							return true;
						}
						try {
							ai.idx = Integer.parseInt(strSuffix);
							return true;
						} 
						catch (Exception ex) {
							return false;
						}
					}
				}
			}
			return false;
		}
		return false;
	}
	
	void loadSubCircuit(boolean inputsOnly, String indent)
	{
		if (cir != null) {
			ArrayIndex ai = new ArrayIndex();
			String subType = getToken();
			String subName = getToken();
			Circuit c = null;
			for (int i=0; i<cir.inputNames.size(); i++) {
				String inputName = cir.inputNames.get(i);
				if (inputName.equals(subName)) {
					error("\nError. Subcircuit has same name as input: " + subName + "\n");
				}
			}
			for (int i=0; i<cir.internalCircuits.size(); i++) {
				Circuit ci = cir.internalCircuits.get(i);
				if (ci.name.equals(subName)) {
					if (!inputsOnly) {
						error("\nError. Duplicate subcircuit name: " + subName + "\n");
					}
					c = ci;
					break;
				}
			}
			if (c == null) {
				c = new Circuit();
				c.load(subType, indent+"  ");
				c.name = subName;
				cir.internalCircuits.add(c);
			}
			
			// connect up inputs to internal circuits
			for (int i=0; i<c.inputNames.size(); i++) {
				String inName = getToken();
				if (inName.equals("}")) {
					error("Error. Incomplete circuit inputs loading circuit '" + cir.type + ".txt'");
				}
				//
				if (inName.equals("...")) {
					cir.isCombinatorial = false;
					break;
				}
				else if (getArrayAndIndex(ai, inName)) {
					c.inputArrays[i] = ai.arr;
					c.inputArrayIndices[i] = ai.idx;
				}
				else if (inName.equals("0") || inName.equals("1")) {
					c.inputs[i] = Integer.parseInt(inName);
				}
				else {
					error("\nError. Could not find input '" + 
						inName + "' loading circuit '" + cir.type + ".txt'\n");
				}
			}
		}
	}
	
	void loadOutputs()
	{
		if (cir != null) {
			ArrayIndex ai = new ArrayIndex();
			
			for (int i=0; i<cir.outputNames.size(); i++) {
				String outputName = getToken();
				if (outputName==null || outputName.equals("}")) {
					error("Error. Incomplete circuit outputs for circuit type '" + cir.type + ".txt'");
				}
				
				if (getArrayAndIndex(ai, outputName)) {
					cir.outputArrays[i] = ai.arr;
					cir.outputArrayIndices[i] = ai.idx;
				}
				else {
					error("\nError. Could not find output '" + 
						outputName + "' loading circuit '" + cir.type + ".txt'\n");
				}
			}
		}
	}
	
	void loadTruthTable()
	{
		if (cir != null) {
			String token;
			int numInputs = cir.inputNames.size();
			int numOutputs = cir.outputNames.size();
			int numRows = (int)(Math.pow(2, numInputs));
			cir.truthTable = new int[numRows];
			
			for (int i=0; i<numRows; i++) {
				int index=0;
				int j=0;
				while (j<numInputs) {
					token = getToken();
					if (token == null) error("Could not load truth table.");
					for (int k=0; k<token.length(); k++) {
						if (token.charAt(k) == '1') {
							index |= (1<<j);
							j++;
						}
						else if (token.charAt(k) == '0') {
							j++;
						}
					}
				}
				//
				int value=0;
				j=0;
				while (j<numOutputs) {
					token = getToken();
					if (token == null) error("Could not load truth table.");
					for (int k=0; k<token.length(); k++) {
						if (token.charAt(k) == '1') {
							value |= (1<<j);
							j++;
						}
						else if (token.charAt(k) == '0') {
							j++;
						}
					}
				}
				//
				cir.truthTable[index] = value;
				//System.out.println(value);
			}
		}
	}
	
	static int loadCount = 0;
	boolean loadFromFile(String circuitType, String indent)
	{
		if (cir != null) {
			//System.out.println(indent + circuitType);
			//System.out.print(".");
			//System.out.print("\rLoading: " + circuitType + "        ");
			System.out.print("\rCircuits loaded: " + (loadCount++) + "  ");
			
			cir.type = circuitType;
			openCircuitFile(circuitType);
			String token;

			
			while ((token = getToken()) != null) {
				if (token.equals("inputNames")) {
					loadStringList(cir.inputNames);
					cir.inputArrays = new int[cir.inputNames.size()][];
					cir.inputArrayIndices = new int[cir.inputNames.size()];
					cir.inputs = new int[cir.inputNames.size()];
				}
				else if (token.equals("outputNames")) {
					loadStringList(cir.outputNames);
					cir.outputArrays = new int[cir.outputNames.size()][];
					cir.outputArrayIndices = new int[cir.outputNames.size()];
					cir.outputs = new int[cir.outputNames.size()];
				}
				else if (token.equals("circuit")) {
					loadSubCircuit(false, indent);
				}
				else if (token.equals("circuitInputs")) {
					loadSubCircuit(true, indent);
				}
				else if (token.equals("outputs")) {
					loadOutputs();
				}
				else if (token.equals("truthTable")) {
					loadTruthTable();
				}
				else if (token.equals("propagationDelay")) {
					ArrayList<String> props = new ArrayList<String>();
					loadStringList(props);
					cir.propagationDelay = new int[cir.outputNames.size()];
					for (int i=0; i<cir.propagationDelay.length; i++) {
						cir.propagationDelay[i] = Integer.parseInt(props.get(i));
					}
				}
			}
			scan.close();
			scan = null;
			extraToken = null;
			
			if (cir.isCombinatorial && cir.truthTable == null 
					&& cir.inputs.length <= MAX_TRUTH_TABLE_INPUTS 
					&& cir.outputs.length <= MAX_TRUTH_TABLE_OUTPUTS) {
				cir.calculateTruthTable();
			}
			
			return false;
		}
		return false;
	}
	
	public void runTestCase(String fileName)
	{
		if (cir != null) {
			openCircuitFile(fileName); // Open the test file
			String token;
			int numInputs = cir.inputs.length;
			int numOutputs = cir.outputs.length;
			int desiredOutputs[] = new int[numOutputs];
			int totalErrors = 0;
			
			System.out.println("\nRunning test cases from " + fileName + ".txt\n");
			cir.printInputOutputNames();
			
			while ((token = getToken()) != null) {
				if (token.equals("testCase")) {
					int inCount = 0;
					while (inCount < numInputs && scan.hasNext()) {
						String s = scan.next();
						for (int i=0; i<s.length(); i++) {
							char c = s.charAt(i);
							if (c == '0' || c == '1') {
								cir.inputs[inCount] = c - '0';
								inCount++;
							}
						}
					}
					int outCount = 0;
					while (outCount < numOutputs && scan.hasNext()) {
						String s = scan.next();
						for (int i=0; i<s.length(); i++) {
							char c = s.charAt(i);
							if (c == '0' || c == '1') {
								desiredOutputs[outCount] = c - '0';
								outCount++;
							}
						}
					}
					if (inCount != numInputs || outCount != numOutputs) {
						error("\nError. Incomplete test case.\n");
					}
					cir.simulate();
					
					System.out.print("\nInput  : ");
					for (int i=0; i<numInputs; i++) {
						if (i%4==0) System.out.print(" ");
						if (i%32==0 && i>0) System.out.print("\n         ");
						System.out.print(cir.inputs[i]);
					}
					System.out.print("\nDesired: ");
					for (int i=0; i<numOutputs; i++) {
						if (i%4==0) System.out.print(" ");
						System.out.print(desiredOutputs[i]);
					}
					System.out.print("\nActual : ");
					for (int i=0; i<numOutputs; i++) {
						if (i%4==0) System.out.print(" ");
						System.out.print(cir.outputs[i]);
					}
					int numErrors = 0;
					for (int i=0; i<numOutputs; i++) {
						if (cir.outputs[i] != desiredOutputs[i]) numErrors++;
					}
					totalErrors += numErrors;
					if (numErrors > 0) {
						System.out.print("\nErrors : ");
						for (int i=0; i<numOutputs; i++) {
							if (i%4==0) System.out.print(" ");
							if (cir.outputs[i] != desiredOutputs[i]) System.out.print("^");
							else System.out.print(" ");
						}
						System.out.print("\nError Names: ");
						for (int i=0; i<numOutputs; i++) {
							if (cir.outputs[i] != desiredOutputs[i]) {
								System.out.print(cir.outputNames.get(i) + " ");
							}
						}
					}
					System.out.println("");
				}
			}
			if (totalErrors > 0) {
				System.out.println("\n***** " + totalErrors + " ERRORS FOUND. *****");
			}
			else {
				System.out.println("\n***** ALL TEST CASES PASSED. *****");
			}
		}
	}
	
}
