
// Circuit.java
// by David Cline

import java.util.ArrayList;

class ArrayIndex
{
	int[] arr;
	int idx;
}

public class Circuit
{
	//---------------------------------------------------------------------//
	// Parsing Stuff
	//---------------------------------------------------------------------//
	
	
	
	//---------------------------------------------------------------------//
	// Circuit Definition
	//---------------------------------------------------------------------//
	
	// Circuit type with inputs and outputs
	String type;
	ArrayList<String> inputNames = new ArrayList<String>();
	ArrayList<String> outputNames = new ArrayList<String>();
	
	// Internal representation of circuit (either truth table or subcircuits)
	boolean isCombinatorial;
	int truthTable[];
	ArrayList<Circuit> internalCircuits = new ArrayList<Circuit>();
	
	//---------------------------------------------------------------------//
	// Circuit Instance
	//---------------------------------------------------------------------//
	
	// Input and Output Setup
	String name;
	int inputArrays[][];      // arrays of input from other circuit instances
	int inputArrayIndices[];  // indices into the input arrays
	int inputs[];             // input values
	//
	int outputArrays[][];     // arrays of input from other circuit instances
	int outputArrayIndices[]; // indices into the input arrays
	int outputs[];            // output values
	
	int propagationDelay[];   // propagationDelay for each output
	
	//---------------------------------------------------------------------//
	// Basic Functions
	//---------------------------------------------------------------------//
	
	Circuit() { isCombinatorial = true; }
	int getInputIndex(String s) { return inputNames.indexOf(s); }
	int getOutputIndex(String s) { return outputNames.indexOf(s); }
	
	//---------------------------------------------------------------------//
	// SIMULATION FUNCTIONS
	//---------------------------------------------------------------------//
	
	void calculateTruthTable()
	{
		for (int i = 0; i < internalCircuits.size(); i++) {
			if (!internalCircuits.get(i).isCombinatorial) return;
		}
	
		//System.out.println("Calculating truth table for " + type);
		int numInputs = inputNames.size();
		int numOutputs = outputNames.size();
		int numRows = (int)(Math.pow(2, numInputs));
		int TT[] = new int[numRows];
		
		for (int r=0; r<numRows; r++) {
			for (int i=0; i<numInputs; i++) {
				inputs[i] = (r>>i) & 0x1;
			}
			simulate();
			int val=0;
			for (int i=0; i<numOutputs; i++) {
				val |= outputs[i] << i;
			}
			//System.out.println("  " + r + " " + val);
			TT[r] = val;
		}
		truthTable = TT;
	}
	
	void gatherInputs()
	{
		for (int i = 0; i < inputs.length; i++) {
			if (inputArrays[i] != null) {
				inputs[i] = inputArrays[i][inputArrayIndices[i]];
			}
		}
	}
	
	void simulate()
	{
		// Use truth table if we have one
		if (truthTable != null) {
			int row = 0;
			for (int i=0; i<inputs.length; i++) {
				row |= inputs[i] << i;
			}
			int out = truthTable[row];
			for (int i=0; i<outputs.length; i++) {
				outputs[i] = (out >> i) & 0x1;
			}
		}
		// Otherwise simulate the internal circuits
		else {
			for (int i=0; i<internalCircuits.size(); i++) {
				Circuit c = internalCircuits.get(i);
				c.gatherInputs();
				c.simulate();
			}
			for (int i=0; i<outputs.length; i++) {
				outputs[i] = outputArrays[i][outputArrayIndices[i]];
			}
		}
	}
	
	void simulatePropagationDelay()
	{
		// If we have a truth table, use tabular propagation delay from input file
		if (internalCircuits.size() == 0) {
			int maxInputProp = 0;
			for (int i=0; i<inputs.length; i++) {
				if (inputs[i] > maxInputProp) maxInputProp = inputs[i];
			}
			for (int i=0; i<outputs.length; i++) {
				if (propagationDelay != null) {
					outputs[i] = propagationDelay[i] + maxInputProp;
				}
				else {
					outputs[i] = 1 + maxInputProp;
				}
			}
		}
		// Otherwise simulate the delay of the internal circuits
		else {
			for (int i=0; i<internalCircuits.size(); i++) {
				Circuit c = internalCircuits.get(i);
				c.gatherInputs();
				c.simulatePropagationDelay();
			}
			for (int i=0; i<outputs.length; i++) {
				outputs[i] = outputArrays[i][outputArrayIndices[i]];
			}
		}
	}
	
	//---------------------------------------------------------------------//
	// TEST FUNCTIONS
	//---------------------------------------------------------------------//
	
	void test(String testFile) {
		CircuitParser cp = new CircuitParser(this);
		cp.runTestCase(testFile);
	}

	@SuppressWarnings("unused")
	void printTruthTable(int maxRows)
	{
		int numInputs = inputNames.size();
		int numOutputs = outputNames.size();
		int rowsToPrint = (int)(Math.pow(2, numInputs));
		if (rowsToPrint > maxRows || numInputs >= 32) rowsToPrint = maxRows;
		
		System.out.println("\nTRUTH TABLE:");
		printInputOutputNames();
		
		for (int r=0; r<rowsToPrint; r++) {
			for (int i=0; i<numInputs; i++) {
				int sh = (numInputs-1-i);
				inputs[i] = (r>>sh) & 0x1;
			}
			simulate();
			if (r%8 == 0) System.out.println();
			printInputOutput();
		}
	}
	
	void printInputOutputNames() 
	{
		int numInputs = inputNames.size();
		int numOutputs = outputNames.size();
		
		for (int i=0; i<numInputs; i++) {
			System.out.print(inputNames.get(i) + " ");
		}
		System.out.print("| ");
		for (int i=0; i<numOutputs; i++) {
			System.out.print(outputNames.get(i) + " ");
		}
		System.out.println("");
	}
	
	void printInputOutput()
	{
		int numInputs = inputNames.size();
		int numOutputs = outputNames.size();
		
		for (int i=0; i<numInputs; i++) {
			System.out.print(inputs[i]);
			if (i%4==3) System.out.print(" ");
		}
		System.out.print(" | ");
		for (int i=0; i<numOutputs; i++) {
			System.out.print(outputs[i]);
			if (i%4==3) System.out.print(" ");
		}
		System.out.println("");
	}
	
	void load(String circuitName, String indent) {
		CircuitParser cp = new CircuitParser(this);
		cp.loadFromFile(circuitName, indent);
		
	}
	
	
	
}
