# CircuitSim
This is the interpreter for text files containing circuit information.
Used to simulate gates and circuits in very low level computer hardware.   
The original program was written by Prof. David Cline at 
Oklahoma State University for his computer systems classes.

# Download
Pre-compiled jar files can be found here:  
[Downloads](https://gitlab.com/d5c4b3/CircuitSim/wikis/Downloads)

# How to
The [manual](https://gitlab.com/d5c4b3/CircuitSim/wikis/Manual) contains all the
information needed to use CircuitSim